<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Employee extends Model
{
    use HasFactory;
    protected $fillable = ['full_name','salary','birthdate','laboratory_id'];
    public function laboratories() : HasMany
    {
        return $this->hasMany(Laboratory::class);
    }
}
