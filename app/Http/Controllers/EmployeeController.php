<?php

namespace App\Http\Controllers;
use App\Actions\Employee\DeleteEmployeeAction;
use App\Actions\Employee\GetEmployeeAction;
use App\Actions\Employee\PatchEmployeeAction;
use App\Actions\Employee\PostEmployeeAction;
use App\Actions\Employee\PutEmployeeAction;
use App\Http\Requests\EmployeeRequest;
use App\Http\Resources\EmployeeResource;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function patch(int $id, EmployeeRequest $request, PatchEmployeeAction $action) {
        $employee = new EmployeeResource(
            $action->execute($id, $request->validated())
        );

        return response()->json($employee, 200);
    }

    public function get(int $id, GetEmployeeAction $action) {
        $employee = new EmployeeResource($action->execute($id));

        return response()->json($employee, 200);
    }

    public function post(EmployeeRequest $request, PostEmployeeAction $action) {
        $employee = new EmployeeResource(
            $action->execute($request->validated())
        );

        return response()->json($employee, 201);
    }

    public function delete(int $id, DeleteEmployeeAction $action)
    {
        $employee = new EmployeeResource($action->execute($id));
        return response()->json($employee, 200);
    }

    public function put(int $id, EmployeeRequest $request, PutEmployeeAction $action) {
        $employee = new EmployeeResource(
            $action->execute($id, $request->validated())
        );

        return response()->json($employee, 200);
    }
}
