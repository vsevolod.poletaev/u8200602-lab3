<?php

namespace App\Http\Controllers;

use App\Actions\Laboratory\DeleteLaboratoryAction;
use App\Actions\Laboratory\GetLaboratoryAction;
use App\Actions\Laboratory\PatchLaboratoryAction;
use App\Actions\Laboratory\PostLaboratoryAction;
use App\Actions\Laboratory\PutLaboratoryAction;
use App\Http\Requests\LaboratoryRequest;
use App\Http\Resources\LaboratoryResource;
use Illuminate\Http\Request;

class LaboratoryController extends Controller
{
    public function patch(int $id, LaboratoryRequest $request, PatchLaboratoryAction $action) {
        $laboratory = new LaboratoryResource(
            $action->execute($id, $request->validated())
        );

        return response()->json($laboratory, 200);
    }

    public function get(int $id, GetLaboratoryAction $action) {
        $laboratory = new LaboratoryResource($action->execute($id));

        return response()->json($laboratory, 200);
    }

    public function post(LaboratoryRequest $request, PostLaboratoryAction $action) {
        $laboratory = new LaboratoryResource(
            $action->execute($request->validated())
        );

        return response()->json($laboratory, 201);
    }

    public function delete(int $id, DeleteLaboratoryAction $action)
    {
        $laboratory = new LaboratoryResource($action->execute($id));
        return response()->json($laboratory, 200);
    }

    public function put(int $id, LaboratoryRequest $request, PutLaboratoryAction $action) {
        $laboratory = new LaboratoryResource(
            $action->execute($id, $request->validated())
        );

        return response()->json($laboratory, 200);
    }
}
