<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'data'=> [
                'id'=>$this->id,
                'full_name'=>$this->full_name,
                'salary'=>$this->salary,
                'birthdate'=>$this->birthdate,
                'laboratory_id'=>$this->laboratory_id,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ],
            'errors' => [

            ],
            'meta' => [
                
            ]
        ];
    }
}