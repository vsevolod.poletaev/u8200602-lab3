<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LaboratoryResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'data' => [
                'id'=>$this->id,
                'department'=>$this->department,
                'cabinet'=>$this->cabinet,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ],
            'errors' => [

            ],
            'meta' => [
                
            ]
        ];
        
    }
}
