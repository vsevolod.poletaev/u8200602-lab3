<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'full_name'=>'string|max:45',
            'salary'=> 'integer|min:15000',
            'birthdate'=>['date_format:Y-m-d','before:2006-01-01','after:1923-01-01'],
            'laboratory_id'=>'required',
        ];
    }
}
