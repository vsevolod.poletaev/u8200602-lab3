<?php

namespace App\Actions\Employee;

use App\Models\Employee;

class DeleteEmployeeAction
{
    public function execute(int $id):Employee
    {
        $employee = Employee::findOrFail($id);
        $employee->delete();
        return $employee;
    }
}