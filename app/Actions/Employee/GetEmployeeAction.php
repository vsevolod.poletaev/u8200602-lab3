<?php

namespace App\Actions\Employee;

use App\Models\Employee;

class GetEmployeeAction
{
    public function execute(int $id):Employee
    {
        $employee = Employee::findOrFail($id);
        return $employee;
    }
}