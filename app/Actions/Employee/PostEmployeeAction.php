<?php

namespace App\Actions\Employee;

use App\Models\Employee;

class PostEmployeeAction
{
    public function execute(array $fields):Employee
    {
        $employee = Employee::create($fields);
        return $employee;
    }
}