<?php

namespace App\Actions\Employee;

use App\Models\Employee;

class PutEmployeeAction
{
    public function execute(int $id, array $fields):Employee
    {
        $employee = Employee::findOrFail($id);
        $employee->update($fields);
        return $employee;
    }
}