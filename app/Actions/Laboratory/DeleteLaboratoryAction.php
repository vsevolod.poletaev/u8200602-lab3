<?php

namespace App\Actions\Laboratory;

use App\Models\Laboratory;

class DeleteLaboratoryAction
{
    public function execute(int $id):Laboratory
    {
        $laboratory = Laboratory::findOrFail($id);
        $laboratory->delete();
        return $laboratory;
    }
}