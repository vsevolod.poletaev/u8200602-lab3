<?php

namespace App\Actions\Laboratory;

use App\Models\Laboratory;

class PatchLaboratoryAction
{
    public function execute(int $id, array $fields):Laboratory
    {
        $laboratory = Laboratory::findOrFail($id);
        $laboratory->update($fields);
        return $laboratory;
    }
}