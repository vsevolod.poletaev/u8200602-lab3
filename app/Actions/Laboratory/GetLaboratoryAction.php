<?php

namespace App\Actions\Laboratory;

use App\Models\Laboratory;

class GetLaboratoryAction
{
    public function execute(int $id):Laboratory
    {
        $laboratory = Laboratory::findOrFail($id);
        return $laboratory;
    }
}