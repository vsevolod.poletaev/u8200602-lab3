<?php

namespace App\Actions\Laboratory;

use App\Models\Laboratory;

class PostLaboratoryAction
{
    public function execute(array $fields):Laboratory
    {
        $laboratory = Laboratory::create($fields);
        return $laboratory;
    }
}