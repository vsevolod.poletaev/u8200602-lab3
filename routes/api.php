<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\LaboratoryController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('v1/employees/{id}', [EmployeeController::class, 'get']);
Route::patch('v1/employees/{id}', [EmployeeController::class, 'patch']);
Route::post('v1/employees/', [EmployeeController::class, 'post']);
Route::delete('v1/employees/{id}', [EmployeeController::class, 'delete']);
Route::put('v1/employees/{id}', [EmployeeController::class, 'put']);

Route::get('v1/laboratories/{id}', [LaboratoryController::class, 'get']);
Route::patch('v1/laboratories/{id}', [LaboratoryController::class, 'patch']);
Route::post('v1/laboratories/', [LaboratoryController::class, 'post']);
Route::delete('v1/laboratories/{id}', [LaboratoryController::class, 'delete']);
Route::put('v1/laboratories/{id}', [LaboratoryController::class, 'put']);
