<?php

use App\Models\Laboratory;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(Tests\TestCase::class);

it('can create a laboratory', function () {
    $attributes = Laboratory::factory()->raw();

    $response = $this->postJson('/api/v1/laboratories', $attributes);
    $response->assertStatus(201);
    $this->assertDatabaseHas('laboratories', $attributes);
});

it('can get a laboratory', function () {
    $laboratory = Laboratory::factory()->create();

    $response = $this->getJson("/api/v1/laboratories/{$laboratory['id']}");
    $response->assertStatus(200)
    ->assertJsonPath('data.id', $laboratory['id'])
    ->assertJsonPath('data.department', $laboratory['department'])
    ->assertJsonPath('data.cabinet', $laboratory['cabinet']);
    $laboratory->delete();
});

it('cant get a laboratory', function () {
    $response = $this->getJson("/api/v1/laboratories/0");
    $response->assertStatus(404);
});


it('can put a laboratory', function () {
    $laboratory = Laboratory::factory()->create();
    $updatedDepartment = ['department' => 3];
    $response = $this->putJson("/api/v1/laboratories/{$laboratory['id']}", $updatedDepartment);
    $response->assertStatus(200);
    $this->assertDatabaseHas('laboratories', $updatedDepartment);
    $laboratory->delete();
});

it('can patch a laboratory', function () {
    $laboratory = Laboratory::factory()->create();
    $updatedDepartment = ['department' => 3];
    $response = $this->patchJson("/api/v1/laboratories/{$laboratory['id']}", $updatedDepartment);
    $response->assertStatus(200);
    $this->assertDatabaseHas('laboratories', $updatedDepartment);
    $laboratory->delete();
});

it('can delete a laboratory', function () {
    $laboratory = Laboratory::factory()->create();
    $response = $this->deleteJson("/api/v1/laboratories/{$laboratory['id']}");
    $response->assertStatus(200);
}); 