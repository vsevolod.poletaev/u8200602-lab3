<?php

use App\Models\Employee;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(Tests\TestCase::class);

it('can create a employee', function () {
    $attributes = Employee::factory()->raw();

    $response = $this->postJson('/api/v1/employees', $attributes);
    $response->assertStatus(201);
    $this->assertDatabaseHas('employees', $attributes);
});

it('can get a employee', function () {
    $employee = Employee::factory()->create();

    $response = $this->getJson("/api/v1/employees/{$employee['id']}");
    $response->assertStatus(200)
    ->assertJsonPath('data.id', $employee['id'])
    ->assertJsonPath('data.full_name', $employee['full_name'])
    ->assertJsonPath('data.salary', $employee['salary'])
    ->assertJsonPath('data.birthdate', $employee['birthdate'])
    ->assertJsonPath('data.laboratory_id', $employee['laboratory_id']);
    $employee->delete();
});

it('cant get a employee', function () {
    $response = $this->getJson("/api/v1/employees/0");
    $response->assertStatus(404);
});


it('can put a employee', function () {
    $employee = Employee::factory()->create();
    $updatedFullName = [
        'full_name' => 'full name namiov',
        'laboratory_id' => 1
    ];
    $response = $this->putJson("/api/v1/employees/{$employee['id']}", $updatedFullName);
    $response->assertStatus(200);
    $this->assertDatabaseHas('employees', $updatedFullName);
    $employee->delete();
});

it('can patch a employee', function () {
    $employee = Employee::factory()->create();
    $updatedFullName = [
        'full_name' => 'full name namiov',
        'laboratory_id' => 1
    ];
    $response = $this->patchJson("/api/v1/employees/{$employee['id']}", $updatedFullName);
    $response->assertStatus(200);
    $this->assertDatabaseHas('employees', $updatedFullName);
    $employee->delete();
});

it('can delete a employee', function () {
    $employee = Employee::factory()->create();
    $response = $this->deleteJson("/api/v1/employees/{$employee['id']}");
    $response->assertStatus(200);
});
