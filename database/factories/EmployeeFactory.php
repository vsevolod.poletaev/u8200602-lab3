<?php

namespace Database\Factories;

use App\Models\Laboratory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as Faker;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Employee>
 */
class EmployeeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $this->faker = Faker::create();
        $name = $this->faker->text(40);
        return [
            'full_name'=>$name,
            'salary'=> $this->faker->numberBetween(15000,150000),
            'birthdate'=>$this->faker->dateTimeBetween('-100 years', '-16 years')->format("Y-m-d"),
            'laboratory_id'=>Laboratory::inRandomOrder()->first()->id,
        ];
    }
}
